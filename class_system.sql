
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `auser` varchar(30) DEFAULT NULL,
  `apass` varchar(30) DEFAULT NULL,
  `aphone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of admin
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES (1, 'admin', '123456', '123');
INSERT INTO `admin` VALUES (3, 'test', '123456', '123456');
COMMIT;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cname` varchar(30) DEFAULT NULL,
  `cbig` int(10) DEFAULT NULL,
  `clocate` varchar(50) DEFAULT NULL,
  `ctype` int(10) DEFAULT NULL,
  `cset` varchar(255) DEFAULT NULL,
  `cmonday` varchar(50) DEFAULT NULL,
  `ctueday` varchar(50) DEFAULT NULL,
  `cwesday` varchar(50) DEFAULT NULL,
  `cthusday` varchar(50) DEFAULT NULL,
  `cfriday` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of class
-- ----------------------------
BEGIN;
INSERT INTO `class` VALUES (13, 'd-333', 23, '教学楼a', 2, NULL, 'a 1-2', 'p 1-2', 'a 3-4', 'a 1-2', 'a 3-4');
INSERT INTO `class` VALUES (14, 'd-210', 35, '教学楼a', 2, '饮水机', 'a 1-2', 'a 3-4', 'a 3-4', 'a 3-4', 'a 3-4');
COMMIT;

-- ----------------------------
-- Table structure for classtype
-- ----------------------------
DROP TABLE IF EXISTS `classtype`;
CREATE TABLE `classtype` (
  `id` int(10) NOT NULL,
  `ctype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of classtype
-- ----------------------------
BEGIN;
INSERT INTO `classtype` VALUES (1, '教学教室');
INSERT INTO `classtype` VALUES (2, '实验教室');
INSERT INTO `classtype` VALUES (3, '工程教室');
INSERT INTO `classtype` VALUES (4, '会议教室');
COMMIT;

-- ----------------------------
-- Table structure for reserve
-- ----------------------------
DROP TABLE IF EXISTS `reserve`;
CREATE TABLE `reserve` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rname` varchar(20) NOT NULL,
  `rclassname` varchar(20) NOT NULL,
  `rclasstime` varchar(20) DEFAULT NULL,
  `rstime` varchar(20) DEFAULT NULL,
  `rstatus` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) DEFAULT NULL,
  `usex` varchar(10) DEFAULT NULL,
  `upass` varchar(40) DEFAULT NULL,
  `uphone` varchar(20) DEFAULT NULL,
  `tname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (11, 'zs', '男', '123', '13346666666', '张三');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
