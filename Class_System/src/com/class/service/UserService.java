import java.sql.SQLException;
import java.util.List;

public class UserService {


  private UserDao userDao=new UserDao();



  public User getone(String user,String pass) throws SQLException {

    if(userDao.selectone(user,pass)==null){
      return null;
    }
    return userDao.selectone(user,pass);
  }

  public int adduser(User user) throws SQLException {
    return  userDao.addone(user);
  }

  public List<User> getall() throws SQLException {
    return userDao.selectall();
  }

  public void edituser(int id,String name,String phone) throws SQLException {
    userDao.edituser(id,name,phone);
  }

  public void deleteuser(int id) throws SQLException {
    userDao.deleteuser(id);
  }

  public User getuserbyid(int id) throws SQLException {
    return userDao.selectuserbyid(id);
  }

  public List getuserbyname(String name) throws SQLException {
    return userDao.selectbyname(name);
  }




}
