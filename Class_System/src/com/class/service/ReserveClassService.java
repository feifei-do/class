import java.sql.SQLException;
import java.util.List;

public class ReserveClassService {


  public ReserveClassDao reserveClassDao=new ReserveClassDao();




  public int addreserveclass(String username,String classname,String classtime,String stime) throws SQLException {
    return reserveClassDao.addreserveclass(username, classname, classtime, stime);

  }

  public List<Reserve> getall() throws SQLException {
    return reserveClassDao.selectallreserve();
  }

  public int updatereserve(int id) throws SQLException {
    return reserveClassDao.updatereservetatus(id);
  }


}
