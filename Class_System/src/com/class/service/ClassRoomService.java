import java.sql.SQLException;
import java.util.List;

public class ClassRoomService {

  ClassRoomDao classDao=new ClassRoomDao();


  public List<ClassRoom> getallclass() throws SQLException {
    return classDao.selectall();
  }

  public int deleteclassroom(int id) throws SQLException {
    return classDao.deleteclassroombyid(id);
  }

  public ClassRoom getoneclassbyid(int id) throws SQLException {
    return classDao.selectbyid(id);
  }

  public int updateclassroom(ClassRoom classRoom) throws SQLException {
    return classDao.editclass(classRoom);
  }

  public int addclassroom(ClassRoom classRoom) throws SQLException {
    return classDao.addclass(classRoom);
  }

  public List<ClassWeek> getallclassname() throws SQLException {
    return classDao.getallclassname();
  }


}
