package dto;

public class TableListDto {

  private int code;
  private String message;
  private Object data;
  private int count;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public TableListDto(int code, String message, Object data, int count) {
    this.code = code;
    this.message = message;
    this.data = data;
    this.count = count;
  }

  public TableListDto() {
  }
}
