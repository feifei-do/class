public class ClassRoom {

  private int id;
  private String cname;
  private int cbig;
  private String clocate;
  private int ctype;
  private String cset;
  private String cmonday;
  private String ctueday;
  private String cwesday;
  private String cthusday;
  private String cfriday;

  public ClassRoom() {
  }

  public ClassRoom(int id, String cname, int cbig, String clocate, int ctype, String cset, String cmonday, String ctueday, String cwesday, String cthusday, String cfriday) {
    this.id = id;
    this.cname = cname;
    this.cbig = cbig;
    this.clocate = clocate;
    this.ctype = ctype;
    this.cset = cset;
    this.cmonday = cmonday;
    this.ctueday = ctueday;
    this.cwesday = cwesday;
    this.cthusday = cthusday;
    this.cfriday = cfriday;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCname() {
    return cname;
  }

  public void setCname(String cname) {
    this.cname = cname;
  }

  public int getCbig() {
    return cbig;
  }

  public void setCbig(int cbig) {
    this.cbig = cbig;
  }

  public int getCtype() {
    return ctype;
  }

  public void setCtype(int ctype) {
    this.ctype = ctype;
  }

  public String getCset() {
    return cset;
  }

  public void setCset(String cset) {
    this.cset = cset;
  }

  public String getCmonday() {
    return cmonday;
  }

  public void setCmonday(String cmonday) {
    this.cmonday = cmonday;
  }

  public String getCtueday() {
    return ctueday;
  }

  public void setCtueday(String ctueday) {
    this.ctueday = ctueday;
  }

  public String getCwesday() {
    return cwesday;
  }

  public void setCwesday(String cwesday) {
    this.cwesday = cwesday;
  }

  public String getCthusday() {
    return cthusday;
  }

  public void setCthusday(String cthusday) {
    this.cthusday = cthusday;
  }

  public String getCfriday() {
    return cfriday;
  }

  public void setCfriday(String cfriday) {
    this.cfriday = cfriday;
  }

  public String getClocate() {
    return clocate;
  }

  public void setClocate(String clocate) {
    this.clocate = clocate;
  }

  @Override
  public String toString() {
    return "ClassRoom{" +
      "id=" + id +
      ", cname='" + cname + '\'' +
      ", cbig=" + cbig +
      ", clocate='" + clocate + '\'' +
      ", ctype='" + ctype + '\'' +
      ", cset='" + cset + '\'' +
      ", cmonday='" + cmonday + '\'' +
      ", ttueday='" + ctueday + '\'' +
      ", cwesday='" + cwesday + '\'' +
      ", cthusday='" + cthusday + '\'' +
      ", cfriday='" + cfriday + '\'' +
      '}';
  }
}
