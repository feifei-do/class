public class Reserve {

  private int id;
  private String rname;
  private String rclassname;
  private String rclasstime;
  private String rstime;
  private String rstatus;

  public Reserve() {
  }

  public Reserve(int id, String rname, String rclassname, String rclasstime, String rstime, String rstatus) {
    this.id = id;
    this.rname = rname;
    this.rclassname = rclassname;
    this.rclasstime = rclasstime;
    this.rstime = rstime;
    this.rstatus = rstatus;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getRname() {
    return rname;
  }

  public void setRname(String rname) {
    this.rname = rname;
  }

  public String getRclassname() {
    return rclassname;
  }

  public void setRclassname(String rclassname) {
    this.rclassname = rclassname;
  }

  public String getRclasstime() {
    return rclasstime;
  }

  public void setRclasstime(String rclasstime) {
    this.rclasstime = rclasstime;
  }

  public String getRstime() {
    return rstime;
  }

  public void setRstime(String rstime) {
    this.rstime = rstime;
  }

  public String getRstatus() {
    return rstatus;
  }

  public void setRstatus(String rstatus) {
    this.rstatus = rstatus;
  }

  @Override
  public String toString() {
    return "Reserve{" +
      "id=" + id +
      ", rname='" + rname + '\'' +
      ", rclassname='" + rclassname + '\'' +
      ", rclasstime='" + rclasstime + '\'' +
      ", rstime='" + rstime + '\'' +
      ", rstatus='" + rstatus + '\'' +
      '}';
  }
}
