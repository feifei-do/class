import java.util.List;

public class ClassWeek {


  private String cname;
  private List weeks;

  public ClassWeek(String cname, List weeks) {
    this.cname = cname;
    this.weeks = weeks;
  }

  public ClassWeek() {
  }

  public String getCname() {
    return cname;
  }

  public void setCname(String cname) {
    this.cname = cname;
  }

  public List getWeeks() {
    return weeks;
  }

  public void setWeeks(List weeks) {
    this.weeks = weeks;
  }

  @Override
  public String toString() {
    return "ClassWeek{" +
      "cname='" + cname + '\'' +
      ", weeks=" + weeks +
      '}';
  }
}
