public class User {

  private int uid;
  private String uname;
  private String usex;
  private String upass;
  private String uphone;
  private String tname;

  public User(int uid, String uname, String usex, String upass, String uphone, String tname) {
    this.uid = uid;
    this.uname = uname;
    this.usex = usex;
    this.upass = upass;
    this.uphone = uphone;
    this.tname = tname;
  }

  public User() {
  }

  public int getUid() {
    return uid;
  }

  public void setUid(int uid) {
    this.uid = uid;
  }

  public String getUname() {
    return uname;
  }

  public void setUname(String uname) {
    this.uname = uname;
  }

  public String getUsex() {
    return usex;
  }

  public void setUsex(String usex) {
    this.usex = usex;
  }

  public String getUpass() {
    return upass;
  }

  public void setUpass(String upass) {
    this.upass = upass;
  }

  public String getUphone() {
    return uphone;
  }

  public void setUphone(String uphone) {
    this.uphone = uphone;
  }

  public String getTname() {
    return tname;
  }

  public void setTname(String tname) {
    this.tname = tname;
  }

  @Override
  public String toString() {
    return "User{" +
      "uid=" + uid +
      ", uname='" + uname + '\'' +
      ", usex='" + usex + '\'' +
      ", upass='" + upass + '\'' +
      ", uphone='" + uphone + '\'' +
      ", tname='" + tname + '\'' +
      '}';
  }
}
