import org.apache.commons.dbutils.BaseResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class UserDao {

  public  QueryRunner queryRunner;
  private Connection collection;

  {
     queryRunner=new QueryRunner();
     collection = DataSourceUtil.getconn();
  }


  public User selectone(String user, String pass) throws SQLException {

    Connection collection = DataSourceUtil.getconn();
    String sql = "select * from user where uname=? and upass=? ";
    User user1=null;
    PreparedStatement preparedStatement = collection.prepareStatement(sql);
    ResultSet resultSet =null;
    try {
      preparedStatement.setString(1,user);
      preparedStatement.setString(2,pass);
      resultSet = preparedStatement.executeQuery();
      while (resultSet.next()){
       user1= new User(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }finally {
      resultSet.close();
      preparedStatement.close();
      collection.close();
    }
    return user1;
  }

  public int addone(User user) throws SQLException {
    String sql="insert into user(uname,usex,upass,uphone,tname) values(?,?,?,?,?)";
    int row= queryRunner.update(collection,sql,user.getUname(),user.getUsex(),user.getUpass(),user.getUphone(),user.getTname());
    return row;
  }

  public List<User> selectall() throws SQLException {
    String sql="select * from user";
    return queryRunner.query(collection,sql, new BeanListHandler<User>(User.class));
  }

  public int edituser(int id,String uname,String phone) throws SQLException {
    String sql="update user set uname=?,uphone=? where uid=?";
    return queryRunner.execute(collection,sql,uname,phone,id);
  }

  public int deleteuser(int id) throws SQLException {
    String sql="delete from user where uid=?";
    return queryRunner.execute(collection,sql,id);
  }

  public User selectuserbyid(int id) throws SQLException {
    String sql="select * from user where uid=?";
    return queryRunner.query(collection,sql,id,new BeanHandler<User>(User.class));

  }

  public List<User> selectbyname(String name) throws SQLException {
    String sql="select * from user where uname like ?";
    return queryRunner.query(collection,sql,"%"+name+"%",new BeanListHandler<User>(User.class));

  }


}
