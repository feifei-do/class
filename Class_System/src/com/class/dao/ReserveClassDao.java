import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ReserveClassDao {


  public QueryRunner queryRunner;
  private Connection collection;

  {
    queryRunner=new QueryRunner();
    collection = DataSourceUtil.getconn();
    try {
      collection.setAutoCommit(true);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }



  public int addreserveclass(String name,String classname,String classtime,String starttime) throws SQLException {
    String sql="insert into reserve(rname,rclassname,rclasstime,rstime,rstatus) values(?,?,?,?,'已预约')";
    return queryRunner.update(collection,sql,name,classname,classtime,starttime);
  }
  public List<Reserve> selectallreserve() throws SQLException {
    String sql="select * from reserve";
    return queryRunner.query(collection,sql,new BeanListHandler<Reserve>(Reserve.class));
  }

  public int updatereservetatus(int id) throws SQLException {
    String sql="update reserve set rstatus='预约成功' where id=?";
    return queryRunner.update(collection,sql,id);
  }




}
