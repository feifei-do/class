import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassRoomDao {


  public QueryRunner queryRunner;
  private Connection collection;

  {
    queryRunner=new QueryRunner();
    collection = DataSourceUtil.getconn();
    try {
      collection.setAutoCommit(true);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }


  public int deleteclassroombyid(int id) throws SQLException {
    String sql="delete from class where id = ?";
    return queryRunner.execute(collection,sql,id);

  }

  public List<ClassRoom> selectall() throws SQLException {
    String sql="select * from class";
    return queryRunner.query(collection,sql, new BeanListHandler<ClassRoom>(ClassRoom.class));
  }

  public ClassRoom selectbyid(int id) throws SQLException {
    String sql="select * from class where id=?";
    return queryRunner.query(collection,sql,new BeanHandler<ClassRoom>(ClassRoom.class),id);
  }




  public int edituser(int id,String uname,String phone) throws SQLException {
    String sql="update user set uname=?,uphone=? where uid=?";
    return queryRunner.execute(collection,sql,uname,phone,id);
  }

  public int deleteuser(int id) throws SQLException {
    String sql="delete from user where uid=?";
    return queryRunner.execute(collection,sql,id);
  }

  public User selectuserbyid(int id) throws SQLException {
    String sql="select * from user where uid=?";
    return queryRunner.query(collection,sql,id,new BeanHandler<User>(User.class));

  }

  public List<User> selectbyname(String name) throws SQLException {
    String sql="select * from user where uname like ?";
    return queryRunner.query(collection,sql,"%"+name+"%",new BeanListHandler<User>(User.class));
  }

  public int editclass(ClassRoom classRoom) throws SQLException {
    String sql="update class set cname=?,cbig=?,ctype=?,cset=?,cmonday=?,ctueday=?,cwesday=?,cthusday=?,cfriday=? where id=?";
    return queryRunner.update(collection,sql,classRoom.getCname(),classRoom.getCbig(),classRoom.getCtype(),classRoom.getCset(),classRoom.getCmonday(),classRoom.getCtueday(),classRoom.getCwesday(),classRoom.getCthusday(),classRoom.getCfriday(),classRoom.getId());
  }

  public int addclass(ClassRoom classRoom) throws SQLException {
    String sql="insert into class(cname,cbig,clocate,ctype,cset,cmonday,ctueday,cwesday,cthusday,cfriday) values(?,?,?,?,?,?,?,?,?,?)";
    return queryRunner.update(collection,sql,classRoom.getCname(),classRoom.getCbig(),classRoom.getClocate(),classRoom.getCtype(),classRoom.getCset(),classRoom.getCmonday(),classRoom.getCtueday(),classRoom.getCwesday(),classRoom.getCthusday(),classRoom.getCfriday());
  }

  public List<ClassWeek> getallclassname() throws SQLException {
    List list=new ArrayList();
    String sql="select cname ,cmonday,ctueday,cwesday,cthusday,cfriday from class";
    PreparedStatement preparedStatement = collection.prepareStatement(sql);
    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()){
      ClassWeek classWeek=new ClassWeek();
      classWeek.setCname(resultSet.getString(1));
      classWeek.setWeeks(Arrays.asList(resultSet.getString(2),resultSet.getString(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6)));
      list.add(classWeek);
    }
    return list;
  }


}
