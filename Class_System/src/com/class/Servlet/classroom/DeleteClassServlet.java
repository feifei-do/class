import com.alibaba.fastjson.JSONObject;
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "DeleteClassServlet",urlPatterns = "/system/class/delete")
public class DeleteClassServlet extends HttpServlet {

    public ClassRoomService classRoomService=new ClassRoomService();



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter writer = response.getWriter();
      String id=request.getParameter("id");
      System.out.println(id);
      try {
        int row=classRoomService.deleteclassroom(Integer.parseInt(id));
        if(row>0){
          writer.print(JSONObject.toJSON(new Commresult(200,"删除成功")));
        }else {
          writer.print(JSONObject.toJSON(new Commresult(444,"删除失败")));
        }
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }

    }
}
