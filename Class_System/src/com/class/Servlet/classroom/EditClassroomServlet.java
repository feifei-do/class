import com.alibaba.fastjson.JSONObject;
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "EditClassroomServlet",urlPatterns = "/system/classroom/edit")
public class EditClassroomServlet extends HttpServlet {

  ClassRoomService classRoomService=new ClassRoomService();



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter writer = response.getWriter();
      String id=request.getParameter("id");
      String cname=request.getParameter("cname");
      String cbig=request.getParameter("cbig");
      String clocate=request.getParameter("clocate");
      String ctype=request.getParameter("ctype");
      String cset=request.getParameter("cset");
      String mon=request.getParameter("cmonday");
      String tue=request.getParameter("ctueday");
      String wes=request.getParameter("cwesday");
      String thus=request.getParameter("cthusday");
      String fri=request.getParameter("cfriday");
      ClassRoom classRoom=new ClassRoom(Integer.parseInt(id),cname,Integer.parseInt(cbig),clocate,Integer.parseInt(ctype),cset,mon,tue,wes,thus,fri);
      try {
        int i = classRoomService.updateclassroom(classRoom);
        if(i>0){
          writer.print(JSONObject.toJSON(new Commresult(200,"修改成功")));
        }else {
          writer.print(JSONObject.toJSON(new Commresult(444,"修改成功")));
        }
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
