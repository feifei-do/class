import com.alibaba.fastjson.JSONObject;
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "EditClassServlet",urlPatterns = "/system/class/edit")
public class EditClassServlet extends HttpServlet {

   public ClassRoomService classRoomService=new ClassRoomService();



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter writer = response.getWriter();
      String classid=request.getParameter("id");
      try {
        ClassRoom classRoom = classRoomService.getoneclassbyid(Integer.parseInt(classid));
        if(classRoom!=null){
          writer.print(JSONObject.toJSON(new Commresult(200,"查询成功",classRoom)));
        }else{
          writer.print(JSONObject.toJSON(new Commresult(444,"查询成功")));
        }
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }
    }
}
