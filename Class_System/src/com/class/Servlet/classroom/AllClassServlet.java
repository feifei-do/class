import com.alibaba.fastjson.JSONObject;
import dto.TableListDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "AllClassServlet",urlPatterns = "/system/class/getall")
public class AllClassServlet extends HttpServlet {

  public ClassRoomService classRoomService=new ClassRoomService();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      PrintWriter writer = response.getWriter();
      try {
        List<ClassRoom> getallclass = classRoomService.getallclass();
        List<ClassWeek> getallclassname = classRoomService.getallclassname();
        HttpSession session = request.getSession();
        session.setAttribute("classweek",getallclassname);
        writer.print(JSONObject.toJSON(new TableListDto(0,"查询成功",getallclass,getallclass.size())));
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }


    }
}
