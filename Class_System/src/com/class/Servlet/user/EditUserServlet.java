import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "EditUserServlet",urlPatterns = "/system/user/edit")
public class EditUserServlet extends HttpServlet {

    UserService userService=new UserService();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String uid=request.getParameter("uid");
      try {
        User user = userService.getuserbyid(Integer.parseInt(uid));
        HttpSession session = request.getSession();
        session.setAttribute("user",user);
        response.sendRedirect("/Class_System/edituser.jsp");
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }


    }
}
