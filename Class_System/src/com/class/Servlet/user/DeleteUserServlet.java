import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DeleteUserServlet", urlPatterns = "/system/user/delete")
public class DeleteUserServlet extends HttpServlet {
    UserService userService=new UserService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      String uid=request.getParameter("uid");
      System.out.println();
      try {
        userService.deleteuser(Integer.parseInt(uid));
        HttpSession session = request.getSession();
        session.setAttribute("userlist",userService.getall());
        response.sendRedirect("/Class_System/user.jsp");
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }

    }
}
