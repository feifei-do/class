import com.alibaba.fastjson.JSONObject;
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "UserServlet",urlPatterns = "/system/user/login")
public class UserServlet extends HttpServlet {

    UserService userService=new UserService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter writer = response.getWriter();
      String user=request.getParameter("user");
      String pass=request.getParameter("pass");
      try {
        if(userService.getone(user,pass)!=null){

          writer.print(JSONObject.toJSON(new Commresult(200,"登录成功",userService.getone(user,pass))));
        }else{
          writer.print("登录失败");
        }
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }

    }
}
