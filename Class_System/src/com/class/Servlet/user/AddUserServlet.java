import com.alibaba.fastjson.JSONObject;
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "AddUserServlet",urlPatterns = "/system/user/add")
public class AddUserServlet extends HttpServlet {

  private UserService userService=new UserService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      PrintWriter writer = response.getWriter();
      String uname=request.getParameter("uname");
      String usex=request.getParameter("usex");
      String upass=request.getParameter("upass");
      String tname=request.getParameter("tname");
      String uphone=request.getParameter("uphone");
      int row= 0;
      try {
        row = userService.adduser(new User(0,uname,usex,upass,tname,uphone));
        if(row>0){
          writer.print(JSONObject.toJSON(new Commresult(200,"注册成功")));

        }else {
          writer.print(JSONObject.toJSON(new Commresult(444,"注册失败")));
        }

      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
