import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "EditUserServlet2",urlPatterns = "/system/user/edit2")
public class EditUserServlet2 extends HttpServlet {

    UserService userService=new UserService();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      HttpSession session = request.getSession();
      String uid=request.getParameter("uid");
      String uname=request.getParameter("uname");
      String uphone=request.getParameter("uphone");

      try {
        userService.edituser(Integer.parseInt(uid),uname,uphone);
        session.setAttribute("userlist",userService.getall());
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
