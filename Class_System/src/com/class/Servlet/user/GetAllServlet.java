import com.alibaba.fastjson.JSONObject;
import dto.TableListDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "GetAllServlet",urlPatterns = "/system/user/getall")
public class GetAllServlet extends HttpServlet {

  UserService userService=new UserService();



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

       HttpSession session = request.getSession();
      try {
        String uname=request.getParameter("uname");
        if(uname.equals("")){
          List getuserbyname = userService.getall();
          session.setAttribute("userlist",getuserbyname);
          response.sendRedirect("/Class_System/user.jsp");
        }else{
          List<User> getall = userService.getuserbyname(uname);
          session.setAttribute("userlist",getall);
          response.sendRedirect("/Class_System/user.jsp");
        }
//        writer.print(JSONObject.toJSON(new TableListDto(0,"查询成功",getall,getall.size())));
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//      PrintWriter writer = response.getWriter();
      doPost(request,response);

    }
}
