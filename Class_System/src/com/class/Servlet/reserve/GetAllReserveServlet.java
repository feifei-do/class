import com.alibaba.fastjson.JSONObject;
import dto.TableListDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "GetAllReserveServlet" ,urlPatterns = "/system/reserve/getall")
public class GetAllReserveServlet extends HttpServlet {
    public ReserveClassService reserveClassService=new ReserveClassService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter writer = response.getWriter();
      try {
        List<Reserve> alllist = reserveClassService.getall();
        writer.print(JSONObject.toJSON(new TableListDto(0,"查询成功",alllist,alllist.size())));
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }


    }
}
