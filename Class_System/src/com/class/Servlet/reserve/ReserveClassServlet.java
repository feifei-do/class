
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "ReserveClassServlet",urlPatterns = "/system/reserve/addclass")
public class ReserveClassServlet extends HttpServlet {

    ReserveClassService reserveClassService=new ReserveClassService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter writer = response.getWriter();

      String uname = request.getParameter("uname");
      String cname=request.getParameter("cname");
      String ctime=request.getParameter("ctime");
      String stime=request.getParameter("stime");
      try {
        int i = reserveClassService.addreserveclass(uname, cname, ctime, stime);
        if(i>0){
          writer.print(new Commresult(200,"预约成功"));
        }else {
          writer.print(new Commresult(444,"预约失败"));
        }
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }
}
