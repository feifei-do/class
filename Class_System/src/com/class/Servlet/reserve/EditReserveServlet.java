import com.alibaba.fastjson.JSONObject;
import dto.Commresult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "EditReserveServlet")
public class EditReserveServlet extends HttpServlet {

    ReserveClassService reserveClassService=new ReserveClassService();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String id=request.getParameter("id");
      PrintWriter writer = response.getWriter();
      try {
        int row = reserveClassService.updatereserve(Integer.parseInt(id));
        if(row>0){
          writer.println(JSONObject.toJSON(new Commresult(200,"修改成功")));
        }
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }


    }
}
