<%--
  Created by IntelliJ IDEA.
  User: guofeifei
  Date: 2020/12/31
  Time: 7:49 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改用户</title>
    <link rel="stylesheet" href="static/css/bootstrap.css">
    <link href="static/css/layui.css" rel="stylesheet">
    <script src="static/js/jquery-3.5.1.js"></script>
    <script src="static/js/bootstrap.js"></script>
</head>


<style>

  #editform{
    margin-top: 30px;
    width: 400px;
    margin-left: 30px;
  }




</style>

<body>
<div id="editform">
<form class="layui-form" action="system/user/edit2" method="post">
  <div class="layui-form-item">
    <label class="layui-form-label">用户id</label>
    <div class="layui-input-block">
      <input id="uid" type="text" name="uid" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input" readonly>
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">用户名</label>
    <div class="layui-input-block">
      <input id="uname" type="text" name="uname" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">手机号</label>
    <div class="layui-input-block">
      <input id="uphone" type="text" name="uphone" lay-verify="required"  placeholder="请输入" autocomplete="off" class="layui-input">
    </div>
  </div>
  <button class="btn btn-success" style="margin-left: 340px">提交</button>
</form>
</div>

<script>
  $(function () {
    $("#uid").val(${user.uid});
    <%--$("#uname").attr("value",${user.uname})--%>
    $("#uname").val("${user.uname}");
    $("#uphone").val(${user.uphone});
  })

</script>
<script src="//res.layui.com/layui/dist/layui.js" charset="utf-8"></script>
<script>

  layui.use(['form', 'layedit', 'laydate'], function() {
    var form = layui.form;
  });
</script>



</body>

</html>
