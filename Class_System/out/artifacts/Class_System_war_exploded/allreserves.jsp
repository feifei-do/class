<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>教室管理</title>
  <link href="static/css/layui.css" rel="stylesheet" media="all">
  <link href="static/css/bootstrap.css" rel="stylesheet">
  <link href="static/css/layui.css" rel="stylesheet" media="all">
  <script type="text/javascript" src="static/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="static/js/bootstrap.js"></script>
</head>
<body>
<div style="padding: 20px; background-color: #F2F2F2;">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md12">
      <div class="layui-card">
        <div class="layui-card-header">教室管理</div>
        <div class="layui-card-body">

          <form action="" method="post">
            <div class="layui-row">
              <div class="layui-form-item layui-col-sm6">
                <label class="layui-form-label">条件</label>
                <div class="layui-input-block">
                  <input type="text" name="uname" lay-verify="title" autocomplete="off" placeholder="请根据用户名查询条件" class="layui-input">
                </div>
              </div>
              <button class="layui-btn layui-btn-warm layui-btn-radius layui-col-sm1" style="margin-left: 10px" id="selectall">查询</button>
            </div>
          </form>


          <table class="layui-table" lay-data="{width: 850, height:500, url:'system/reserve/getall', id:'idTest'}" lay-filter="demo">
            <thead>
            <tr>
              <th lay-data="{field:'id', width:80, sort: true, fixed: true}">id</th>
              <th lay-data="{field:'rname', width:100,fixed: true}">预约人名字</th>
              <th lay-data="{field:'rclassname', width:100, sort: true,fixed: true}">预约教室名字</th>
              <th lay-data="{field:'rclasstime', width:100,fixed: true}">预约教室时间</th>
              <th lay-data="{field:'rstime', width:160,fixed: true}">预约日期</th>
              <th lay-data="{field:'rstatus', width:100,fixed: true}">预约状态</th>
              <th lay-data="{width:200, align:'center', toolbar: '#barDemo'}">操作</th>
            </tr>
            </thead>
          </table>

          <script type="text/html" id="barDemo">
            <a class="layui-btn layui-btn-xs" lay-event="goto">审核提交</a>
            <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
          </script>
          <script type="text/javascript" src="static/js/layui.all.js" charset="UTF-8"></script>
          <script>
            layui.use(['table','layer'], function(){
              var table = layui.table,
                layer=layui.layer;
              //监听表格复选框选择
              table.on('checkbox(demo)', function(obj){
                console.log(obj)
              });
              //监听工具条
              table.on('tool(demo)', function(obj){
                var data = obj.data;
                if(obj.event === 'del'){
                  console.log(data.id+"=========");
                  $.get("system/class/delete",{"id":data.id},function (res) {
                    if(res.code===200){
                      window.location.href="/classroom.jsp";
                    }else{
                      alert("删除失败");
                    }
                  })
                } else if(obj.event === 'edit'){
                  $.get('system/class/edit',{"id":data.id},function (res) {
                    if(res.code===200){
                      window.sessionStorage.setItem("editclass",JSON.stringify(res.data));
                      window.location.href="editclass.jsp";
                    }else{
                      alert("修改失败");
                    }
                  });
                }else if(obj.event === 'goto'){
                  console.log(data.id);
                  // $.get('system/css/edit',{},function (res) {
                  //
                  // })


                }
              });
            });
          </script>

        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>

