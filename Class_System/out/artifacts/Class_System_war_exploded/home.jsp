
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
  <link href="static/css/layui.css" rel="stylesheet" media="all">
  <script type="text/javascript" src="static/js/jquery-3.5.1.js"></script>
</head>
<body class="layui-layout-body">
<script>
           var tname=window.sessionStorage.getItem('name');
           $("#tname").html(tname);
           $("#logout").click(function () {

             window.sessionStorage.clear();
             window.location.href="/index.jsp";
           })

    $(function () {

      $(".aside a").on("click",function(){
      var address =$(this).attr("data-src");
      $("iframe").attr("src",address);
      });
       var frame = $("#aa");
       var frameheight = $(window).height();
       frame.css("height",frameheight);
  })
</script>



<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
    <div class="layui-logo">教室租赁管理系统</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
      <li class="layui-nav-item"><a href="">后台管理</a></li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item">
        <a href="javascript:;">
          <img src="//tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg" class="layui-nav-img">
          <p id="tname"></p>
        </a>
        <dl class="layui-nav-child">
          <dd><a href="">基本资料</a></dd>
          <dd><a href="">安全设置</a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="index.jsp">logout</a></li>
    </ul>
  </div>

  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree layui-inline aside" lay-filter="demo" style="margin-right: 10px;">
        <li class="layui-nav-item layui-nav-itemed">
          <a href="javascript:;">用户管理</a>
          <dl class="layui-nav-child">
            <dd><a  data-src="user.jsp" target="_top">用户查询</a></dd>
            <dd><a  data-src="admin.jsp" target="_top">管理员查询</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">教室管理</a>
          <dl class="layui-nav-child">
            <dd><a data-src="classroom.jsp" target="_top">教室查询</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">订单管理</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">订单查询</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">教室预约</a>
          <dl class="layui-nav-child">
            <dd><a data-src="reserveclass.jsp">预约</a></dd>
            <dd><a data-src="allreserves.jsp">预约记录查询</a></dd>
          </dl>
        </li>
      </ul>
    </div>
  </div>

  <div class="layui-body">
    <!-- 内容主体区域 -->
    <iframe frameborder="0" scrolling="yes" style="width: 100%" src="" id="aa">
    </iframe>
  </div>

  <div class="layui-footer">
    <p>内蒙古工业大学</p>
  </div>
</div>
<script type="text/javascript" src="static/js/layui.all.js" charset="UTF-8"></script>
<script>
  //JavaScript代码区域
  layui.use('element', function(){
    var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
    //监听导航点击
    element.on('nav(demo)', function(elem){
      //console.log(elem)
      // layer.msg(elem.text());
    });
  });
</script>
<script>
</script>

</body>
</html>
