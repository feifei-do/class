/*
 Navicat Premium Data Transfer

 Source Server         : alibabamysql
 Source Server Type    : MySQL
 Source Server Version : 50647
 Source Host           : 39.97.230.144:3306
 Source Schema         : class_system

 Target Server Type    : MySQL
 Target Server Version : 50647
 File Encoding         : 65001

 Date: 02/01/2021 12:17:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `aid` int(10) NOT NULL,
  `auser` varchar(30) DEFAULT NULL,
  `apass` varchar(30) DEFAULT NULL,
  `aphnoe` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of admin
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES (1, 'admin', 'addmin', '11223344551');
COMMIT;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cname` varchar(30) DEFAULT NULL,
  `cbig` int(10) DEFAULT NULL,
  `clocate` varchar(50) DEFAULT NULL,
  `ctype` int(10) DEFAULT NULL,
  `cset` varchar(255) DEFAULT NULL,
  `cmonday` varchar(50) DEFAULT NULL,
  `ctueday` varchar(50) DEFAULT NULL,
  `cwesday` varchar(50) DEFAULT NULL,
  `cthusday` varchar(50) DEFAULT NULL,
  `cfriday` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of class
-- ----------------------------
BEGIN;
INSERT INTO `class` VALUES (1, 'b-104', 56, '教学楼a', 1, '空调', 'a 1-2', 'a 3-4', 'p 1-2', '6，8', '1，4');
INSERT INTO `class` VALUES (2, 'd-136', 60, '教学楼b', 1, NULL, 'a 1-2', 'ss', 's', 's', 's');
INSERT INTO `class` VALUES (10, 'd-110', 23, '教学楼b', 1, NULL, 'a 3-4', 'a 3-4', 'a 3-4', 'a 3-4', 'a 3-4');
COMMIT;

-- ----------------------------
-- Table structure for classtype
-- ----------------------------
DROP TABLE IF EXISTS `classtype`;
CREATE TABLE `classtype` (
  `id` int(10) NOT NULL,
  `ctype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of classtype
-- ----------------------------
BEGIN;
INSERT INTO `classtype` VALUES (1, '教学教室');
INSERT INTO `classtype` VALUES (2, '实验教室');
INSERT INTO `classtype` VALUES (3, '工程教室');
INSERT INTO `classtype` VALUES (4, '会议教室');
COMMIT;

-- ----------------------------
-- Table structure for reserve
-- ----------------------------
DROP TABLE IF EXISTS `reserve`;
CREATE TABLE `reserve` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rname` varchar(20) NOT NULL,
  `rclassname` varchar(20) NOT NULL,
  `rclasstime` varchar(20) DEFAULT NULL,
  `rstime` varchar(20) DEFAULT NULL,
  `rstatus` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of reserve
-- ----------------------------
BEGIN;
INSERT INTO `reserve` VALUES (2, 'zz', 'd-136', 'a 1-2', '2021-01-02', '已预约');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) DEFAULT NULL,
  `usex` varchar(10) DEFAULT NULL,
  `upass` varchar(40) DEFAULT NULL,
  `uphone` varchar(20) DEFAULT NULL,
  `tname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'test', '男', '123456', '1234', 'zz');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
