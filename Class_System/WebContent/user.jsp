<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: guofeifei
  Date: 2020/12/31
  Time: 3:41 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
  <link href="static/css/layui.css" rel="stylesheet" media="all">
  <link href="static/css/bootstrap.css" rel="stylesheet">
  <script type="text/javascript" src="static/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="static/js/bootstrap.js"></script>
</head>


<script>

  // $(function () {
  //   var userlist=[];
  //   $.get('system/user/getall',function (res) {
  //     userlist=res.data;
  //   })
  //
  // })

</script>

<body>

<script>



</script>

<div style="padding: 20px; background-color: #F2F2F2;">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md12">
      <div class="layui-card">
        <div class="layui-card-header">用户管理</div>
        <div class="layui-card-body">

          <form action="system/user/getall" method="post">
          <div class="layui-row">
          <div class="layui-form-item layui-col-sm4">
            <label class="layui-form-label">查询条件</label>
            <div class="layui-input-block">
              <input type="text" name="uname" lay-verify="title" autocomplete="off" placeholder="请根据用户名查询条件" class="layui-input">
            </div>
          </div>
          <button class="layui-btn layui-btn-warm layui-btn-radius layui-col-sm1" style="margin-left: 10px" id="selectall">查询</button>
          </div>
          </form>
          <table class="table table-hover">
            <tr>
              <td>id</td>
              <td>用户名</td>
              <td>性别</td>
              <td>电话号</td>
              <td>真实姓名</td>
              <td>操作</td>
            </tr>
            <c:forEach var="item" items="${userlist}">

              <tr>
                <td>${item.uid}</td>
                <td>${item.uname}</td>
                <td>${item.usex}</td>
                <td>${item.uphone}</td>
                <td>${item.tname}</td>
                <td>
                  <a href="system/user/edit?uid=${item.uid}">修改</a>
                  <a href="system/user/delete?uid=${item.uid}">删除</a>
                </td>
              </tr>
            </c:forEach>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="static/js/layui.all.js" charset="UTF-8"></script>

<script>
  $("#selectall").click(function () {
  })

  // layui.use('table', function(){
  //   var table = layui.table;
  //
  //   table.render({
  //     elem: '#test',
  //     url: '/system/user/getall'
  //     ,cols: [[
  //       {field:'uid', width:80, title: 'ID', sort: true}
  //       ,{field:'uname', width:80, title: '用户名'}
  //       ,{field:'usex', width:80, title: '性别', sort: true}
  //       ,{field:'uphone', width:120, title: '手机号'}
  //       ,{field:'tname', title: '真实姓名', width: 100, minWidth: 100}
  //       ,{title: '操作', width: 100, minWidth: 100}//minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
  //     ]]
  //   });
  // });

  //   table.render({
  //     elem: '#test',
  //     cols: [[ //标题栏
  //       {field: 'uid', title: 'ID', width: 80, sort: true}
  //       ,{field: 'uname', title: '用户名', width: 120}
  //       ,{field: 'usex', title: '性别', minWidth: 150}
  //       ,{field: 'uphone', title: '电话号', minWidth: 160}
  //       ,{field: 'tname', title: '真实姓名', width: 80}
  //       ,{title: '操作', width: 120}
  //     ]]
  //     ,cellMinWidth: 80
  //     ,data: userlist//全局定义常规单元格的最小宽度，layui 2.2.1 新增
  //   });
  // });
</script>
</body>
</html>
