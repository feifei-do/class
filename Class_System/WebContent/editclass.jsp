
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/css/bootstrap.css">
    <link href="static/css/layui.css" rel="stylesheet">
    <script src="static/js/jquery-3.5.1.js"></script>
    <script src="static/js/bootstrap.js"></script>
</head>
<body>

<style>

  #editclass{

    margin-left: 100px;
    margin-top: 30px;
    width: 400px;
    height: 100%;
  }



</style>


<div id="editclass">
  <form class="layui-form" lay-filter="example">
    <div class="layui-form-item">
      <label class="layui-form-label">教室id</label>
      <div class="layui-input-block">
        <input type="text" name="id" lay-verify="title" autocomplete="off" class="layui-input" readonly>
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">教室名字</label>
      <div class="layui-input-block">
        <input type="text" name="cname" placeholder="请输入教室名字" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">教室规格</label>
      <div class="layui-input-block">
        <input type="text" name="cbig" placeholder="请输入教室规格" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">教室位置</label>
      <div class="layui-input-block">
        <select name="clocate" lay-filter="aihao">
          <option value=""></option>
          <option value="教学楼a">教学楼a</option>
          <option value="教学楼b">教学楼b</option>
          <option value="教学楼c">教学楼c</option>
          <option value="教学楼d">教学楼d</option>
        </select>
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label">教室类型</label>
      <div class="layui-input-block">
        <select name="ctype" lay-filter="aihao">
          <option value=""></option>
          <option value="1">教学</option>
          <option value="2">实验</option>
          <option value="3">工程</option>
          <option value="4">会议</option>
        </select>
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label">星期一</label>
      <div class="layui-input-block">
        <input type="text" name="cmonday" placeholder="请输入教室规格" autocomplete="off" class="layui-input">
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label">星期二</label>
      <div class="layui-input-block">
        <input type="text" name="ctueday" placeholder="请输入教室规格" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">星期三</label>
      <div class="layui-input-block">
        <input type="text" name="cwesday" placeholder="请输入教室规格" autocomplete="off" class="layui-input">
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label">星期四</label>
      <div class="layui-input-block">
        <input type="text" name="cthusday" placeholder="请输入教室规格" autocomplete="off" class="layui-input">
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label">星期五</label>
      <div class="layui-input-block">
        <input type="text" name="cfriday" placeholder="请输入教室规格" autocomplete="off" class="layui-input">
      </div>

    </div>
    <div class="layui-form-item">
      <div class="layui-input-block">
        <button type="button" class="layui-btn layui-btn-normal" id="LAY-component-form-setval">赋值</button>
        <button type="button" class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
      </div>

      <script src="static/js/layui.all.js" charset="UTF-8"></script>
      <script>
        var x=window.sessionStorage.getItem('editclass');
        var xjson=$.parseJSON(x);


        layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
      ,layer = layui.layer
      ,layedit = layui.layedit
      ,laydate = layui.laydate;

      //监听提交
      form.on('submit(demo1)', function(data){
        $.ajax({
          url: "system/classroom/edit",
          type: "post",
          dataType: "json",
          data: data.field,
          success: function (res) {
             if(res.code===200){
               window.location.href="/Class_System/classroom.jsp";
             }else {
               alert("修改失败");
             }
          }

        })


         // $.post('system/classroom/edit',data.field,function (res) {
         //   console.log(res);
         //   if(res.code===200){
         //     // window.location.href="/classroom.jsp";
         //   }else {
         //     alert("修改失败");
         //   }
         // })
      });

      //表单赋值
      layui.$('#LAY-component-form-setval').on('click', function(){
      form.val('example',xjson);
      });

      });
      </script>

    </div>
  </form>

</div>
</body>
</html>
