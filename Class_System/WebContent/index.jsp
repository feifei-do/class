<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>

    <link rel="stylesheet" href="static/css/bootstrap-theme.css">
    <link rel="stylesheet" href="static/css/bootstrap.css">
    <link rel="stylesheet" href="static/css/global.css">
    <script type="text/javascript" src="static/js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="static/js/bootstrap.js"></script>

  </head>


  <body>
  <style>

    #login{
      width: 380px;
      height: 200px;
      position: absolute;
      top: 40%;
      left: 50%;
      border-radius: 3px;
      transform: translate(-50%,-50%);
    }
    #button{
      display: flex;
      justify-content: space-between;
    }



  </style>

  <div id="login">

    <h3 style="text-align: center">教室管理系统</h3>


    <form class="form-horizontal">
      <div class="form-group">
        <label for="inputuser" class="col-sm-2 control-label">用户:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="inputuser" placeholder="请输入用户名">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword" class="col-sm-2 control-label">密码:</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="inputPassword" placeholder="请输入密码">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10" id="button">
          <button type="button" class="btn btn-default" id="loging">登录</button>
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#user">注册</button>
        </div>
      </div>
    </form>
  </div>

  <div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">注册用户</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="uname" class="control-label">用户名</label>
              <input type="text" class="form-control" id="uname">
            </div>
            <div class="form-group">
              <label for="usex" class="control-label">性别</label>
              <input type="text" class="form-control" id="usex">
            </div>
            <div class="form-group">
              <label for="upass" class="control-label">密码</label>
              <input type="password" class="form-control" id="upass">
            </div>
            <div class="form-group">
              <label for="tname" class="control-label">真实姓名</label>
              <input type="text" class="form-control" id="tname">
            </div>
            <div class="form-group">
              <label for="uphone" class="control-label">电话号</label>
              <input type="text" class="form-control" id="uphone">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
          <button type="button" class="btn btn-primary" id="submit">提交</button>
        </div>
      </div>
    </div>
  </div>


  <script>


    $(function () {

           $("#loging").click(function () {

              var username=$('#inputuser').val();
              var password=$('#inputPassword').val();

                $.get('system/user/login',{"user":username,"pass":password},function (res) {
                    if(res.code==200) {
                      window.sessionStorage.setItem('name',res.data.tname);
                      window.location.href='home.jsp';
                    }else{
                      alert('用户名或密码错误 ，请重新登录');
                      window.location.href='/index.jsp'
                    }
                })
           })

           $("#submit").click(function () {
             var uname=$("#uname").val();
             var usex=$("#usex").val();
             var upass=$("#upass").val();
             var tname=$("#tname").val();
             var uphone=$("#uphone").val();
             $.post('system/user/add',{'uname':uname,'usex':usex,'upass':upass,'tname':tname,'uphone':uphone},function (res) {
                   if(res.code==200){
                     window.location.href="/index.jsp"
                   }else {
                     alert("注册失败");
                   }
             })

           })

    })



  </script>



  </body>
</html>
