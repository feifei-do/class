<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教室管理</title>
  <link href="static/css/layui.css" rel="stylesheet" media="all">
  <link href="static/css/bootstrap.css" rel="stylesheet">
  <link href="static/css/layui.css" rel="stylesheet" media="all">
  <script type="text/javascript" src="static/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="static/js/bootstrap.js"></script>
</head>
<body>
<div style="padding: 20px; background-color: #F2F2F2;">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md12">
      <div class="layui-card">
        <div class="layui-card-header">教室管理</div>
        <div class="layui-card-body">

          <form action="" method="post">
            <div class="layui-row">
              <div class="layui-form-item layui-col-sm6">
                <label class="layui-form-label">条件</label>
                <div class="layui-input-block">
                  <input type="text" name="uname" lay-verify="title" autocomplete="off" placeholder="请根据用户名查询条件" class="layui-input">
                </div>
              </div>
              <button class="layui-btn layui-btn-warm layui-btn-radius layui-col-sm1" style="margin-left: 10px" id="selectall">查询</button>
              <button type="button" class="layui-btn layui-btn-normal layui-btn-radius layui-col-sm1" style="margin-left: 10px" id="addclassroom">添加</button>
            </div>
          </form>


          <table class="layui-table" lay-data="{width: 1150, height:500, url:'system/class/getall', id:'idTest'}" lay-filter="demo">
            <thead>
            <tr>
              <th lay-data="{field:'id', width:80, sort: true, fixed: true}">id</th>
              <th lay-data="{field:'cname', width:80,fixed: true}">教室名</th>
              <th lay-data="{field:'cbig', width:80, sort: true,fixed: true}">教室规格</th>
              <th lay-data="{field:'clocate', width:80,fixed: true}">教室位置</th>
              <th lay-data="{field:'ctype', width:160,fixed: true}">教室类型</th>
              <th lay-data="{field:'cset', width:80,fixed: true}">教室配置</th>
              <th lay-data="{field:'cmonday', width:80,fixed: true}">星期一</th>
              <th lay-data="{field:'ctueday', width:80,fixed: true}">星期二</th>
              <th lay-data="{field:'cwesday', width:80}">星期三</th>
              <th lay-data="{field:'cthusday', width:80}">星期四</th>
              <th lay-data="{field:'cfriday', width:80}">星期五</th>
              <th lay-data="{width:178, align:'center', toolbar: '#barDemo'}">操作</th>
            </tr>
            </thead>
          </table>

          <script type="text/html" id="barDemo">
            <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
          </script>
          <script type="text/javascript" src="static/js/layui.all.js" charset="UTF-8"></script>
          <script>

            $(function () {

              $("#addclassroom").click(function () {
                 window.location.href="/Class_System/addclassroom.jsp";
              })


            })






            layui.use(['table','layer'], function(){
              var table = layui.table,
              layer=layui.layer;
              //监听表格复选框选择
              table.on('checkbox(demo)', function(obj){
                console.log(obj)
              });
              //监听工具条
              table.on('tool(demo)', function(obj){
                var data = obj.data;
                if(obj.event === 'del'){
                  console.log(data.id+"=========");
                  $.get("system/class/delete",{"id":data.id},function (res) {
                    if(res.code===200){
                      window.location.href="/Class_System/classroom.jsp";
                    }else{
                      alert("删除失败");
                    }
                  })
                } else if(obj.event === 'edit'){
                  $.get('system/class/edit',{"id":data.id},function (res) {
                    if(res.code===200){
                       window.sessionStorage.setItem("editclass",JSON.stringify(res.data));
                      window.location.href="/Class_System/editclass.jsp";
                    }else{
                      alert("修改失败");
                    }
                  });
                }
              });
            });
          </script>

        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
