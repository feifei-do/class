<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
  <title>Title</title>
  <link rel="stylesheet" href="static/css/bootstrap.css">
  <link href="static/css/layui.css" rel="stylesheet">
  <link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js"></script>
  <script src="static/js/bootstrap.js"></script>
  <script src="https://cdn.bootcss.com/moment.js/2.24.0/moment-with-locales.js"></script>
  <script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
</head>
<body>

<style>

  #editclass{
    margin-left: 100px;
    margin-top: 30px;
    width: 550px;
    height: 100%;
  }
</style>
<div id="editclass">
  <form class="layui-form" lay-filter="example">


    <div class="layui-form-item">
      <label class="layui-form-label">预约教室</label>
      <div class="layui-input-block">
        <select name="rclassname" lay-verify="required" id="selectclass">
          <c:forEach items="${sessionScope.get('classweek')}" var="item">
            <option value="${item.cname}">${item.cname}</option>
          </c:forEach>
        </select>
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">时间预约</label>
      <div class="layui-input-block">
        <select name="rclasstime" lay-verify="required" id="selecttime">
          <c:forEach items="${sessionScope.get('classweek')}" var="item">
            <p>${item}</p>
            <c:forEach items="${item.weeks}" var="week">
              <option value="${week}">教室名:${item.cname}-可选择时间：${week}</option>
            </c:forEach>
          </c:forEach>
        </select>
      </div>
    </div>

    <div class="layui-form-item">
      <label class="layui-form-label">预约日期</label>
      <div class='input-group date layui-input-block' id='datetimepicker1'>
        <input type='text' class="form-control" name="classtime" />
        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
      </span>
      </div>
    </div>

    <!--指定 date标记-->


    <div class="layui-form-item">
      <div class="layui-input-block">
        <button type="button" class="layui-btn" lay-submit="" lay-filter="demo1">立即预约</button>
      </div>

      <script src="static/js/layui.all.js" charset="UTF-8"></script>
      <script>
        $(function () {

          $('#datetimepicker1').datetimepicker({

            format: 'YYYY-MM-DD',

            locale: moment.locale('zh-cn')

          });


        })




        layui.use(['form', 'layedit', 'laydate'], function(){
          var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

          laydate.render({
            elem: '#test1'
          });
          //监听提交
          form.on('submit(demo1)', function(data){

            var x=window.sessionStorage.getItem("name");
            console.log(x);
            console.log(data);
            $.ajax({
              url: "system/reserve/addclass",
              type: "post",
              dataType: "json",
              data: {
                uname: x,
                cname: data.field.rclassname,
                ctime: data.field.rclasstime,
                stime: data.field.classtime
              },
              success: function (res) {
                if(res.code===200){
                  alert("预约成功");
                  window.location.href="/home.jsp";
                }else {
                  alert("修改失败");
                }
              }

            })
          });


        });
      </script>
    </div>
  </form>

</div>
</body>
</html>

